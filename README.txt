$Id 
-- SUMMARY --

The Drupal Portal Plus Authentication module provides authentication
integration with the Portal Plus SOAP web service

For a full description of the module, visit the project page:
  http://drupal.org/project/ppauth

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/ppauth


-- REQUIREMENTS --

* SOAP Client Drupal Module with nuSOAP library, see:

		-SOAP Client Module: http://drupal.org/project/soapclient
		
	 	-nuSOAP library: http://sourceforge.net/projects/nusoap/files/nusoap/0.7.3/nusoap-0.7.3.zip/download


-- INSTALLATION --

* Enable the module and it's dependencies. 


-- CONFIGURATION --

* Configure the Portal Plus settings at Administer >> Configuration >> Portal Plus Integration:

  - Login URI

    The URI of the portal plus web service e.g: http://portal.mysite.com/app/service.asmx?WSDL

  - Client ID

    The unique ID provided by Portal Plus for your account


-- TROUBLESHOOTING --

* If the authentication is failing you can run a test of your configuration using the
	SOAP Client test page at: Administer >> Configuration >> SOAP Client >> Test/Demo


-- FAQ --

* None as yet


-- CONTACT --

Current maintainers:
* Reece Marsland (reecemarsland) - http://drupal.org/user/536168


This project has been sponsored by:
* The New Digital Partnership
  Web design & development using Drupal agency. 
  Visit http://www.newdigitalpartnership.com for more information.



